package monty;

import org.junit.Before;
import org.junit.Test;

import static monty.MontyHallSimulator.Box.FIRST_BOX;
import static monty.MontyHallSimulator.Box.SECOND_BOX;
import static monty.MontyHallSimulator.Box.THIRD_BOX;
import static org.assertj.core.api.Assertions.assertThat;

public class MontyHallSimulatorTest {

    private MontyHallSimulator montyHallSimulator;

    @Before
    public void before() {
        montyHallSimulator = new MontyHallSimulator();
    }

    @Test
    public void playerChoosesWinningBox() {
        assertThat(montyHallSimulator.simulateSwitch(FIRST_BOX, FIRST_BOX)).isFalse();
    }

    @Test
    public void playerChoosesEmptyBox1() {
        assertThat(montyHallSimulator.simulateSwitch(FIRST_BOX, SECOND_BOX)).isTrue();
    }

    @Test
    public void playerChoosesEmptyBox2() {
        assertThat(montyHallSimulator.simulateSwitch(FIRST_BOX, THIRD_BOX)).isTrue();
    }
}