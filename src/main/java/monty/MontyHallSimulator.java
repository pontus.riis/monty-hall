package monty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static monty.MontyHallSimulator.Box.FIRST_BOX;
import static monty.MontyHallSimulator.Box.SECOND_BOX;
import static monty.MontyHallSimulator.Box.THIRD_BOX;

/**
 * Simulator of the Monty Hall game show problem.
 */
public class MontyHallSimulator {

    enum Box{
        FIRST_BOX,
        SECOND_BOX,
        THIRD_BOX
    }

    private final List<Box> boxes = Collections.unmodifiableList(Arrays.asList(FIRST_BOX, SECOND_BOX, THIRD_BOX));

    /**
     * Simulate one game round when player decides to switch
     *
     * @return if player won
     */
    boolean simulateGameRound() {
        Box playerChosenBox = boxes.get(ThreadLocalRandom.current().nextInt(0, 3));
        Box winningBox = boxes.get(ThreadLocalRandom.current().nextInt(0, 3));

        return simulateSwitch(playerChosenBox, winningBox);
    }

    boolean simulateSwitch(Box playerChosenBox, Box winningBox) {
        List<Box> copyOfBoxes = new ArrayList<>(this.boxes);
        copyOfBoxes.remove(playerChosenBox);

        if (playerChosenBox == winningBox) {
            copyOfBoxes.remove(ThreadLocalRandom.current().nextInt(0, 2));
        } else {
            copyOfBoxes.removeIf(box -> box != winningBox);
        }

        Box boxAfterSwitch = copyOfBoxes.get(0);

        return boxAfterSwitch == winningBox;
    }

    /**
     * Simulates one million game rounds where the player chooses to switch and one million where the player
     * chooses not to switch. Ends by printing the results.
     *
     * @param args how many boxes the game should be played with, default = 3
     */
    public static void main(String[] args) {
        MontyHallSimulator montyHallSimulator = new MontyHallSimulator();

        int numberOfWins = 0;
        int numberOfLoops = 1000_000;
        for (int i = 0; i < numberOfLoops; i++) {
            if (montyHallSimulator.simulateGameRound()) {
                numberOfWins += 1;
            }
        }

        System.out.println("Win percentage: " + (double) numberOfWins / numberOfLoops * 100 + "%");
    }
}
